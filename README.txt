Le fichier s'appelle main.py, pour lancer flask, il faudra:   
    1- Utiliser l'environement virtuel si on a pas flask : source ./flask_env_p3/bin/activate
    1-b : installer les modules présents dans modules.txt si besoin : (avec pip install -r modules.txt)
    2- Spécifier le nom du fichier qui contient l'app : export FLASK_APP=main.py
    3- Lancer Flask avec flask run
    4- l'app est accessible à localhost:5000 et l'api à localhost:5000/srv
ps: * - normalement si l'app n'arrive pas à query l'url sur git, en alternative, elle lit le meme fichier ('./data/srv.txt')
    * $ Egalement les logs sont accessible à ./logs/webApp.log
    (j'écrase les fichiers)
