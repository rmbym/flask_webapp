import os, logging, requests as req, json
from flask import Flask, request as flaskRequest
logging.basicConfig(filename='./logs/webapp.log',filemode='w',level=logging.DEBUG,format='%(asctime)s - [%(levelname)s] %(message)s')
results = []

def getData():
    dataUrl = 'https://gist.githubusercontent.com/tristanlt/1fef698f160c73adc8a07f4aa933aa60/raw/718861bde9ffce8bb55d71444855239bd90888de/extract-srv-appli1.txt'
    dataReq = req.get(dataUrl)
    if int(dataReq.status_code) == 200:
        content = dataReq.text.split('\n')
        logging.debug(F'Querying data from : {dataUrl}')
        for data in content:
            if data == '':
                continue
            item = data.split(',')
            results.append({'srv':item[0],'ip':item[1],'state': True if item[-1]=='up' else False})
        with open('./data/data.txt','w') as file:
            for item in results:
                file.write(f'{item};')
    else:
        logging.warning(F'Could not query {dataUrl}')
        if len(results) != 0:
            logging.debug('results should be empty')
            return
        with open('./data/srv.txt','r') as altFile:
            for line in altFile:
                dataArray = line.split(',')
                results.append({'srv':dataArray[0],'ip':dataArray[1],'state': True if dataArray[-1]=='up\n' else False})

getData()
app = Flask(__name__)
@app.route('/')
def welcome():
    res = '<tbdody>'
    for dict in results:
        res += '<tr><td>'+dict['srv']+'</td>'+'<td>'+dict['ip']+'</td>'+'<td>'+str(dict['state'])+'</td>'+'</tr>'
    return '<!DOCTYPE html><html><body></body><table><thead><tr><th>ServerName</th><th>Ip</th><th>State</th></tr></thead>'+res+'</tbody></table></body></html>'

@app.route('/srv')
def showData():
    return {'data':results}
